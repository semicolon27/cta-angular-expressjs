-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2019 at 04:49 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cta_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_page`
--

CREATE TABLE `about_page` (
  `id` varchar(11) NOT NULL,
  `banner_img` varchar(50) NOT NULL,
  `banner_heading` varchar(20) NOT NULL,
  `area_1_title` varchar(20) NOT NULL,
  `area_1` mediumtext NOT NULL,
  `area_2_title` varchar(20) NOT NULL,
  `area_2` mediumtext NOT NULL,
  `area_3_title` varchar(20) NOT NULL,
  `area_3` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_page`
--

INSERT INTO `about_page` (`id`, `banner_img`, `banner_heading`, `area_1_title`, `area_1`, `area_2_title`, `area_2`, `area_3_title`, `area_3`) VALUES
('99', 'aboutus-banner.jpg', 'ABOUT US', 'WHO ARE WE', 'adalah perusahaan di bidang Informasi Teknologi. Berdiri sejak tahun 2006, kami resmi menjadi sebuah badan usaha pada tahun 20xx.\r\nFour Vision Media hanya berfokus pada solusi IT berupa jasa pembuatan website dan aplikasi berbasis website dengan mayoritas kliennya adalah korporat.\r\n\r\nKami mencoba untuk terus berinovasi dalam hal teknologi, kualitas pekerjaan dan pelayanan purna jual, sehingga kami mampu bersaing dengan perusahaan teknologi lainnya. Banyak inovasi dari kami telah menghasilkan kepercayaan dari para klien.', 'SEJARAH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'VISI DAN MISI', 'Visi kami adalah Menjadi Salah Satu Perusahaan IT dengan tingkat loyalitas pelanggan terbaik di Indonesia<br />\r\n\r\n<h1>Visi kami<\\/h1> adalah:\r\nMenciptakan sistem pelayanan purna jual yang berorientasi pada standard \r\n>Menciptakan budaya kerja yang berorientasi pada semangat berinovasi\r\nMemberikan edukasi teknologi kepada para klien kami');

-- --------------------------------------------------------

--
-- Table structure for table `contact_page`
--

CREATE TABLE `contact_page` (
  `id` varchar(11) NOT NULL,
  `banner_img` varchar(50) NOT NULL,
  `banner_heading` varchar(50) NOT NULL,
  `page_subtitle` text NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `jam_operasional` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_page`
--

INSERT INTO `contact_page` (`id`, `banner_img`, `banner_heading`, `page_subtitle`, `alamat`, `no_hp`, `email`, `jam_operasional`) VALUES
('99', 'header_contact.png', 'Hubungi Kami', 'Membantu mitra bisnis kami untuk membangun bisnis dan fokus menciptakan hubungan yang kuat antara konsumen dan merek di setiap aspek Digital dari kehidupan mereka.', 'Alamat:\r\nRuko Grande 8 H, Jl. Raya Laladon No.2, Laladon, Ciomas, Bogor, Jawa Barat 16610', 'No. Hp : 0813-1111-7899', 'Email: cta@gmail.com', 'Buka Jam 08.00–22.00 WIB');

-- --------------------------------------------------------

--
-- Table structure for table `index_page`
--

CREATE TABLE `index_page` (
  `id` int(11) NOT NULL,
  `banner_img` varchar(50) NOT NULL,
  `banner_heading` varchar(20) NOT NULL,
  `banner_sub_heading` text NOT NULL,
  `area_1_title` text NOT NULL,
  `area_1` mediumtext NOT NULL,
  `area_3_title` text NOT NULL,
  `area_3` mediumtext NOT NULL,
  `area_3_bg` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `index_page`
--

INSERT INTO `index_page` (`id`, `banner_img`, `banner_heading`, `banner_sub_heading`, `area_1_title`, `area_1`, `area_3_title`, `area_3`, `area_3_bg`) VALUES
(99, 'header-index.jpg', 'Selamat Datang', 'Mau Punya Website Berkualitas Dengan Harga Terjangkau ?', 'Mengapa Kita Harus Punya Website ?', 'Jaman sudah berubah semuanya serba online. Mau berbelanja sesuatu lewat online. Mau cari informasi tentang sesuatu lewat online. Jika kita tidak ikut berubah maka cepat atau lambat kita akan tersingkir dari peta persaingan yang semakin hari semakin ketat. untuk tetap bisa bersaing kita harus ikut berubah dan ikut eksis di dunia online. Salah satu caranya dengan memiliki webite untuk usaha kita. Oleh karena itu kami jadir untuk membantu mengonlinekan usaha anda.\r\nKami melayani pembuatan webite untuk perusahaan, toko online, sales mobil, personal blog, sales property, berita online, travel, hotel, restoran, sekolah, pantren, kampus, lembaga khusus, instansi pemerintah, rumah sakit, organisasi, yayasan, komunita, iklan bari, landing page, affiliasi, reseller, MLM, dan lain-lain.', 'Cara Pemesanan Webite', 'Untuk Pemesanan Anda Bisa Menghubungi Kami Lewat Telepon, Whatsapp, Maupun Email.', 'home-area_3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `message_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `message` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`message_id`, `name`, `email`, `message`) VALUES
(1, 'aa', 'aa@aa.com', 'aa');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` varchar(11) NOT NULL,
  `product_name` varchar(200) DEFAULT NULL,
  `product_description` text NOT NULL,
  `product_price` int(11) DEFAULT NULL,
  `image` text NOT NULL COMMENT 'cover image'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_description`, `product_price`, `image`) VALUES
('1', 'Pembuatan Aplikasi Android', 'Aplikasi Mobile Apps Android dan iOS menjadikan Bisnis Anda Profesional, Berkelas dan Kekinian. Buat Pelanggan datang ke Toko Anda, lagi dengan terus datang lagi dan Setia di Aplikasi Anda.', 900000, '189_20190226-android.png'),
('1uav24', 'SEO', 'Agar Website anda mudah terindex google', 99000, '804_20190226-pembuatan-web.png'),
('1uav2f', 'Pembuatan Game Android', 'Game Mobile Apps Android menjadikan skill gaming Anda Profesional, Berkelas dan Kekinian. Buat Pelanggan datang ke Youtube Channel Anda, lagi dengan terus datang lagi dan Setia di Channel Anda.', 2000000, '189_20190226-android.png'),
('3', 'Pembuatan Website Statis', 'Jangkau lebih luas Pelanggan Anda di mana pun dan kapan pun mereka berada. Toko anda 24jam Non Stop melakukan promosi kepada pelanggan anda. Menghemat SDM dan Budget Promosi.', 950000, '804_20190226-pembuatan-web.png');

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE `product_image` (
  `img_id` varchar(11) NOT NULL,
  `product_id` varchar(11) NOT NULL,
  `img` text NOT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_page`
--

CREATE TABLE `product_page` (
  `id` varchar(11) NOT NULL,
  `banner_img` varchar(50) NOT NULL,
  `banner_title` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_page`
--

INSERT INTO `product_page` (`id`, `banner_img`, `banner_title`) VALUES
('99', 'product-banner.jpg', 'Produk Kami');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id_service` varchar(11) NOT NULL,
  `svc_name` varchar(25) NOT NULL,
  `svc_desc` text NOT NULL,
  `image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id_service`, `svc_name`, `svc_desc`, `image`) VALUES
('112', 'Design', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui quaerat fugit quas veniam perferendis repudiandae sequi.', 'design.jpg'),
('142', 'App', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui quaerat fugit quas veniam perferendis repudiandae sequi.', 'app.png'),
('172', 'Support', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui quaerat fugit quas veniam perferendis repudiandae sequi.', 'support.jpg'),
('192', 'Branding', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui quaerat fugit quas veniam perferendis repudiandae sequi.', 'branding.jpg'),
('412', 'Development', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui quaerat fugit quas veniam perferendis repudiandae sequi.', 'development.jpg'),
('612', 'Database', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui quaerat fugit quas veniam perferendis repudiandae sequi.', 'database.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `service_page`
--

CREATE TABLE `service_page` (
  `id` varchar(11) NOT NULL,
  `banner_img` varchar(50) NOT NULL,
  `banner_heading` varchar(50) NOT NULL,
  `page_subtitle` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_page`
--

INSERT INTO `service_page` (`id`, `banner_img`, `banner_heading`, `page_subtitle`) VALUES
('99', 'service-banner.jpg', 'Layanan Kami', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`) VALUES
('admin', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_page`
--
ALTER TABLE `about_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_page`
--
ALTER TABLE `contact_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `index_page`
--
ALTER TABLE `index_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_page`
--
ALTER TABLE `product_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id_service`);

--
-- Indexes for table `service_page`
--
ALTER TABLE `service_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
